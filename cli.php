<?php
/**
 * User: ansaus
 * Date: 15.10.2021
 */
require_once __DIR__.'/bootstrap.php';

use app\command\TournamentCommand;
use Symfony\Component\Console as Console;

$application = new Console\Application('Knight game', '1.0.0');
$application->addCommands([new TournamentCommand()]);
$application->run();

