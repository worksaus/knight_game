<?php
/**
 * User: ansaus
 * Date: 15.10.2021
 */
namespace app\model\character;


class Knight extends AbstractCharacter
{

    public function getName(): string
    {
        return 'Sir '.$this->name;
    }
}
