<?php
/**
 * User: ansaus
 * Date: 15.10.2021
 */
namespace app\model\character;

use app\model\weapon\SelectWeaponBehaviorInterface;
use app\model\weapon\WeaponBehaviorInterface;

abstract class AbstractCharacter
{
    /**
     * @var string
     */
    protected $name;

    /**
     * Life points
     * default range from 1 to 100;
     * @var int
     */
    protected $lifePoints = 100;

    /**
     * @var WeaponBehaviorInterface []
     */
    protected $weaponBehaviors;

    /**
     * @var SelectWeaponBehaviorInterface
     */
    protected $selectWeaponBehavior;

    /**
     * @return int
     */
    public function getLifePoints(): int
    {
        return $this->lifePoints;
    }

    /**
     * @param int $lifePoints
     */
    public function setLifePoints(int $lifePoints): void
    {
        $this->lifePoints = $lifePoints;
        if ($this->lifePoints <= 0) $this->lifePoints = 0;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isDead()
    {
        return ($this->lifePoints <= 0);
    }

    public function addWeaponBehavior(WeaponBehaviorInterface $behavior)
    {
        $this->weaponBehaviors[] = $behavior;
    }

    /**
     * @return WeaponBehaviorInterface[]
     */
    public function getWeaponBehaviors(): array
    {
        return $this->weaponBehaviors;
    }

    /**
     * @return SelectWeaponBehaviorInterface
     */
    public function getSelectWeaponBehavior(): SelectWeaponBehaviorInterface
    {
        return $this->selectWeaponBehavior;
    }

    /**
     * @param SelectWeaponBehaviorInterface $selectWeaponBehavior
     */
    public function setSelectWeaponBehavior(SelectWeaponBehaviorInterface $selectWeaponBehavior): void
    {
        $this->selectWeaponBehavior = $selectWeaponBehavior;
    }

    /**
     * @return WeaponBehaviorInterface|null
     */
    public function selectWeapon()
    {
        return $this->selectWeaponBehavior->selectWeapon($this);
    }

}
