<?php
/**
 * User: ansaus
 * Date: 19.10.2021
 */

namespace app\model\service\render;


interface RendererInterface
{
    public function render($data=[]);
}
