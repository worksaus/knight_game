<?php
/**
 * User: ansaus
 * Date: 19.10.2021
 */

namespace app\model\service\render;


use Symfony\Component\Console\Output\OutputInterface;

class ConsoleRenderer implements RendererInterface
{
    /**
     * @var OutputInterface
     */
    protected $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function render($data = [])
    {
        foreach ($data as $key => $value)
        {
            $this->output->writeln($value);
        }
    }
}
