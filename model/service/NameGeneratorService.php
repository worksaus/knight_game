<?php
/**
 * User: ansaus
 * Date: 16.10.2021
 */
namespace app\model\service;

use app\config\NameConfig;

class NameGeneratorService
{
    public static function getName()
    {
        $names = NameConfig::getNames();
        return $names[rand(0, count($names)-1)];
    }
}
