<?php
namespace app\model\weapon;

use app\model\character\AbstractCharacter;

/**
 * User: ansaus
 * Date: 17.10.2021
 */
class BasicWeaponBehavior implements WeaponBehaviorInterface
{

    public function getHitPoints(int $baseHitPoints)
    {
        return ($baseHitPoints) ? $baseHitPoints : rand(1, 50);
    }

    public function getDamagePoints(int $hitPoints)
    {
        return $hitPoints;
    }

    public function getName()
    {
        return "Fist";
    }
}
