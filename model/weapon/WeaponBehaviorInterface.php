<?php
namespace app\model\weapon;

/**
 * User: ansaus
 * Date: 17.10.2021
 */
interface WeaponBehaviorInterface
{
    public function getName();

    public function getHitPoints(int $baseHitPoints);

    public function getDamagePoints(int $hitPoints);
}
