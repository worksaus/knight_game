<?php
namespace app\model\weapon;

/**
 * User: ansaus
 * Date: 17.10.2021
 */
class MagicWandWeaponBehavior extends BasicWeaponBehavior
{

    public function getHitPoints(int $baseHitPoints)
    {
        return ($baseHitPoints) ? $baseHitPoints * 2 : parent::getHitPoints($baseHitPoints) * 2;
    }

    public function getName()
    {
        return "Magic Wand";
    }
}
