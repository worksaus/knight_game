<?php
/**
 * User: ansaus
 * Date: 19.10.2021
 */

namespace app\model\weapon;


use app\model\character\AbstractCharacter;

interface SelectWeaponBehaviorInterface
{
    /**
     * @param AbstractCharacter $character
     * @return WeaponBehaviorInterface | null
     */
    public function selectWeapon(AbstractCharacter $character);
}
