<?php
/**
 * User: ansaus
 * Date: 19.10.2021
 */

namespace app\model\weapon;


use app\model\character\AbstractCharacter;

class SelectWeaponBehavior implements SelectWeaponBehaviorInterface
{

    /**
     * @param AbstractCharacter $character
     * @return WeaponBehaviorInterface
     */
    public function selectWeapon(AbstractCharacter $character)
    {
        // todo logic in selecting weapon
        $behaviors = $character->getWeaponBehaviors();
        if (is_array($behaviors)&&count($behaviors)) {
            return $behaviors[rand(0, count($behaviors)-1)];
        }
        return null;
    }
}
