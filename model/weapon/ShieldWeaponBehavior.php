<?php
namespace app\model\weapon;

use app\model\character\AbstractCharacter;

/**
 * User: ansaus
 * Date: 17.10.2021
 */
class ShieldWeaponBehavior extends BasicWeaponBehavior
{
    public function getDamagePoints(int $hitPoints)
    {
        return 0;
    }

    public function getName()
    {
        return "Shield";
    }
}
