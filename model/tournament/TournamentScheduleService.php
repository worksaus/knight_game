<?php
/**
 * User: ansaus
 * Date: 19.10.2021
 */

namespace app\model\tournament;


use app\model\character\AbstractCharacter;

class TournamentScheduleService
{
    /**
     * @var DuelInfo []
     */
    protected $duelSchedule;

    /**
     * @param $participants
     * @param $roundNumber
     * @return bool
     */
    public function calcDuelSchedule($participants, $roundNumber)
    {
        $duelKey = 1;
        foreach ($participants as $key => $participant)
        {
            for($i=$key+1;$i<count($participants);$i++)
            {
                if (isset($participant)&&$participant instanceof AbstractCharacter&&isset($participants[$i])&&$participants[$i] instanceof AbstractCharacter)
                {
                    $duelInfo = new DuelInfo(new PlayerFightInfo($participant), new PlayerFightInfo($participants[$i]));
                    $this->duelSchedule[$roundNumber][$duelKey++] = $duelInfo;
                }
            }
        }
        if (isset($this->duelSchedule[$roundNumber])&&is_array($this->duelSchedule[$roundNumber])) {
            return true;
        }
        return false;
    }

    /**
     * @param $roundNumber
     * @param $duelNumber
     * @return DuelInfo | null
     */
    public function getNextDuelInfo($roundNumber, $duelNumber)
    {
        if (isset($this->duelSchedule[$roundNumber][$duelNumber+1])) {
            return $this->duelSchedule[$roundNumber][$duelNumber+1];
        }
        return null;
    }

    /**
     * @param $roundNumber
     * @param $duelNumber
     * @return DuelInfo | null
     */
    public function getDuelInfo($roundNumber, $duelNumber)
    {
        if (isset($this->duelSchedule[$roundNumber][$duelNumber])) {
            return $this->duelSchedule[$roundNumber][$duelNumber];
        }
        return null;
    }

    /**
     * @return DuelInfo[]
     */
    public function getDuelSchedule(): array
    {
        return $this->duelSchedule;
    }

    public function isRoundFinished($roundNumber, $duelNumber)
    {
        return !($this->getNextDuelInfo($roundNumber, $duelNumber));
    }

}
