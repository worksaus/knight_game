<?php
/**
 * User: ansaus
 * Date: 16.10.2021
 */
namespace app\model\tournament;

final class TournamentState
{
    const NOT_STARTED = 'not started';
    const IN_PROCESS = 'in process';
    const FINISHED = 'finished';
}
