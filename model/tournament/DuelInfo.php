<?php
/**
 * User: ansaus
 * Date: 16.10.2021
 */

namespace app\model\tournament;


use app\model\character\AbstractCharacter;
use app\model\weapon\WeaponBehaviorInterface;

class DuelInfo
{

    /**
     * @var PlayerFightInfo
     */
    private $playerOneInfo;

    /**
     * @var PlayerFightInfo
     */
    private $playerTwoInfo;

    public function __construct(PlayerFightInfo $playerOneInfo, PlayerFightInfo $playerTwoInfo)
    {
        $this->playerOneInfo = $playerOneInfo;
        $this->playerTwoInfo = $playerTwoInfo;
    }

    /**
     * @return AbstractCharacter
     */
    public function getPlayerOne(): AbstractCharacter
    {
        return $this->playerOneInfo->getPlayer();
    }

    /**
     * @return AbstractCharacter
     */
    public function getPlayerTwo(): AbstractCharacter
    {
        return $this->playerTwoInfo->getPlayer();
    }

    /**
     * @return WeaponBehaviorInterface | null
     */
    public function getPlayerOneSelectedWeapon()
    {
        return $this->playerOneInfo->getSelectedWeapon();
    }

    /**
     * @return WeaponBehaviorInterface | null
     */
    public function getPlayerTwoSelectedWeapon()
    {
        return $this->playerTwoInfo->getSelectedWeapon();
    }

    /**
     * @return PlayerFightInfo
     */
    public function getPlayerOneInfo(): PlayerFightInfo
    {
        return $this->playerOneInfo;
    }

    /**
     * @return PlayerFightInfo
     */
    public function getPlayerTwoInfo(): PlayerFightInfo
    {
        return $this->playerTwoInfo;
    }


}
