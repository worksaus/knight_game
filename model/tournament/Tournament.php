<?php
/**
 * User: ansaus
 * Date: 16.10.2021
 */

namespace app\model\tournament;


use app\model\character\AbstractCharacter;

class Tournament
{
    const MAX_NUMBER_PARTICIPANTS = 100;

    /**
     * @var string
     * @see TournamentState
     */
    protected $state;

    /**
     * @var TournamentScheduleService
     */
    protected $scheduleService;

    /**
     * @var AbstractCharacter []
     */
    protected $participants;
    /**
     * @var int
     */
    protected $roundNumber;

    /**
     * @var int
     */
    protected $duelNumber;

    /**
     * @var DuelService
     */
    protected $duelService;

    public function __construct($scheduleService = null, $duelService = null)
    {
        $this->state = TournamentState::NOT_STARTED;
        $this->scheduleService = ($scheduleService) ?? new TournamentScheduleService();
        $this->duelService = ($duelService) ?? new DuelService();
        $this->roundNumber = 0;
    }

    public function startNextRound()
    {
        $this->roundNumber++;
        $this->state = TournamentState::IN_PROCESS;
        if (!$this->scheduleService->calcDuelSchedule($this->participants, $this->roundNumber))
        {
            $this->calcFinish();
            return false;
        }
        $this->duelNumber = 0;
        return true;
    }

    /**
     * @return DuelInfo
     */
    public function executeNextDuel()
    {
        $this->duelNumber++;
        $duelInfo = $this->scheduleService->getDuelInfo($this->roundNumber, $this->duelNumber);
        $duelInfo = $this->duelService->fight($duelInfo);
        $this->removeDeadPlayers();
        $this->calcFinish();

        return $duelInfo;
    }

    protected function removeDeadPlayers()
    {
        foreach ($this->participants as $key => $player)
        {
            if($player->isDead()) {
                unset($this->participants[$key]);
                $this->participants = array_values($this->participants);
            }
        }
    }

    protected function calcFinish()
    {
        if (boolval(count($this->participants) <= 1)) {
            $this->state = TournamentState::FINISHED;
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getParticipantNumber(): int
    {
        return is_countable($this->participants) ? count($this->participants) : 0;
    }

    /**
     * @return AbstractCharacter[]
     */
    public function getParticipants(): array
    {
        return $this->participants;
    }

    public function addParticipant(AbstractCharacter $participant)
    {
        if (($this->getParticipantNumber()+1) > self::MAX_NUMBER_PARTICIPANTS) throw new \Exception('too many participants for this tournament');
        $this->participants[] = $participant;
    }

    public function isFinished()
    {
        return boolval($this->state == TournamentState::FINISHED);
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    public function getWinner()
    {
        if ($this->isFinished()) {
            foreach ($this->participants as $participant)
            {
                return $participant;
            }
        }
        return null;
    }
    /**
     * @return int
     */
    public function getRoundNumber(): int
    {
        return $this->roundNumber;
    }
    /**
     * @return int
     */
    public function getDuelNumber(): int
    {
        return $this->duelNumber;
    }

    /**
     * @return TournamentScheduleService
     */
    public function getScheduleService(): TournamentScheduleService
    {
        return $this->scheduleService;
    }

    /**
     * @return int
     */
    public function getNextDuelNumber(): int
    {
        return $this->duelNumber + 1;
    }
}
