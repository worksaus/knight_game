<?php
/**
 * User: ansaus
 * Date: 20.10.2021
 */

namespace app\model\tournament;


use app\model\character\AbstractCharacter;
use app\model\weapon\WeaponBehaviorInterface;

class PlayerFightInfo
{
    /**
     * @var AbstractCharacter
     */
    private $player;
    /**
     * @var int
     */
    private $hitPoints;
    /**
     * @var int
     */
    private $damagePoints;
    /**
     * @var WeaponBehaviorInterface
     */
    private $selectedWeapon;

    public function __construct($player, $hitPoints=null, $damagePoints=null, $selectedWeapon=null)
    {
        $this->player = $player;
        $this->hitPoints = intval($hitPoints);
        $this->damagePoints = intval($damagePoints);
        $this->selectedWeapon = $selectedWeapon;
    }

    /**
     * @return AbstractCharacter
     */
    public function getPlayer(): AbstractCharacter
    {
        return $this->player;
    }

    /**
     * @return int
     */
    public function getHitPoints()
    {
        return $this->hitPoints;
    }

    /**
     * @return int
     */
    public function getDamagePoints()
    {
        return $this->damagePoints;
    }

    /**
     * @return WeaponBehaviorInterface | null
     */
    public function getSelectedWeapon()
    {
        return $this->selectedWeapon;
    }

}
