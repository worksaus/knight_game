<?php
/**
 * User: ansaus
 * Date: 19.10.2021
 */

namespace app\model\tournament;


use app\model\character\Knight;
use app\model\service\NameGeneratorService;
use app\model\service\render\RendererInterface;
use app\model\weapon\BasicWeaponBehavior;
use app\model\weapon\MagicWandWeaponBehavior;
use app\model\weapon\SelectWeaponBehavior;
use app\model\weapon\ShieldWeaponBehavior;

class TournamentService
{
    /**
     * @var RendererInterface
     */
    protected $renderer;

    public function __construct(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @param int $playerNumber
     * @return bool
     * @throws \Exception
     */
    public function executeTournament(int $playerNumber)
    {
        // start new tournament
        $tournament = new Tournament();
        $this->renderer->render(['Please welcome the tournament participants : ']);
        for ($i=1;$i<=$playerNumber;$i++) {
            $participant = new Knight();
            $participant->setName(NameGeneratorService::getName());
            $participant->addWeaponBehavior(new BasicWeaponBehavior());
            $participant->addWeaponBehavior(new MagicWandWeaponBehavior());
            $participant->addWeaponBehavior(new ShieldWeaponBehavior());
            $participant->setSelectWeaponBehavior(new SelectWeaponBehavior());
            $tournament->addParticipant($participant);
            $this->renderer->render(['  '.($i).' : '.$participant->getName()]);
        }
        //
        $this->renderer->render(['Lets begin !']);
        $tournament->startNextRound();
        $roundNumber = $tournament->getRoundNumber();
        $this->renderer->render([$roundNumber.' round start:']);
        while (!$tournament->isFinished()) {
            if ($tournament->getScheduleService()->isRoundFinished($tournament->getRoundNumber(), $tournament->getDuelNumber())) {
                $this->renderer->render([$roundNumber.' round finished']);
                if ($tournament->startNextRound()) {
                    $roundNumber = $tournament->getRoundNumber();
                    $this->renderer->render([$roundNumber.' round start:']);
                }
            }
            $duelInfo = $tournament->getScheduleService()->getNextDuelInfo($tournament->getRoundNumber(), $tournament->getDuelNumber());
            $duelNumber = $tournament->getNextDuelNumber();
            $this->renderer->render(['  '.$duelNumber.' duel start:']);
            $this->renderer->render(['    '.$duelInfo->getPlayerOne()->getName().' will fight against '.$duelInfo->getPlayerTwo()->getName()]);
            $this->renderer->render(['    '.$duelInfo->getPlayerOne()->getName().' has '.$duelInfo->getPlayerOne()->getLifePoints().' life points']);
            $this->renderer->render(['    '.$duelInfo->getPlayerTwo()->getName().' has '.$duelInfo->getPlayerTwo()->getLifePoints().' life points']);
            $duelInfo = $tournament->executeNextDuel();
            $this->renderer->render(['  '.$duelNumber.' duel finished']);
            if ($duelInfo->getPlayerOneSelectedWeapon()) {
                $this->renderer->render(['    '.$duelInfo->getPlayerOne()->getName().' chosen '.$duelInfo->getPlayerOneSelectedWeapon()->getName()]);
            }
            $this->renderer->render(['    '.$duelInfo->getPlayerOne()->getName().' now has '.$duelInfo->getPlayerOne()->getLifePoints().' life points']);
            if ($duelInfo->getPlayerTwoSelectedWeapon()) {
                $this->renderer->render(['    '.$duelInfo->getPlayerTwo()->getName().' chosen '.$duelInfo->getPlayerTwoSelectedWeapon()->getName()]);
            }
            $this->renderer->render(['    '.$duelInfo->getPlayerTwo()->getName().' now has '.$duelInfo->getPlayerTwo()->getLifePoints().' life points']);
            if ($duelInfo->getPlayerOne()->isDead()) {
                $this->renderer->render(['    '.$duelInfo->getPlayerOne()->getName().'\'s dead']);
            }
            if ($duelInfo->getPlayerTwo()->isDead()) {
                $this->renderer->render(['    '.$duelInfo->getPlayerTwo()->getName().'\'s dead']);
            }
        }
        $this->renderer->render(['Tournament has finished!']);
        $winner = $tournament->getWinner();
        if ($winner) {
            $this->renderer->render([$winner->getName().' won !!!']);
        } else {
            $this->renderer->render(['Nobody won !!!']);
        }

        return true;
    }
}
