<?php
/**
 * User: ansaus
 * Date: 19.10.2021
 */

namespace app\model\tournament;


class DuelService
{
    /**
     * @param DuelInfo $duelInfo
     * @return DuelInfo
     */
    public function fight(DuelInfo $duelInfo)
    {
        $playerOneInfo = $duelInfo->getPlayerOneInfo();
        $playerTwoInfo = $duelInfo->getPlayerTwoInfo();

        if (!$playerOneInfo->getPlayer()->isDead() && !$playerTwoInfo->getPlayer()->isDead()) {
            $playerOneHitPoints = $playerOneInfo->getHitPoints();
            $playerTwoHitPoints = $playerTwoInfo->getHitPoints();

            $playerOneWeapon = ($playerOneInfo->getSelectedWeapon()) ?? $playerOneInfo->getPlayer()->selectWeapon();
            $playerOneHitPoints = $playerOneWeapon->getHitPoints($playerOneHitPoints);

            $playerTwoWeapon = ($playerTwoInfo->getSelectedWeapon()) ?? $playerTwoInfo->getPlayer()->selectWeapon();
            $playerTwoHitPoints = $playerTwoWeapon->getHitPoints($playerTwoHitPoints);


            $playerTwoDamagePoints = $playerTwoWeapon->getDamagePoints($playerOneHitPoints);
            $playerOneDamagePoints = $playerOneWeapon->getDamagePoints($playerTwoHitPoints);

            $playerOneInfo->getPlayer()->setLifePoints($playerOneInfo->getPlayer()->getLifePoints() - $playerOneDamagePoints);
            $playerTwoInfo->getPlayer()->setLifePoints($playerTwoInfo->getPlayer()->getLifePoints() - $playerTwoDamagePoints);
        }
        $playerOneFightInfo = new PlayerFightInfo($playerOneInfo->getPlayer(), $playerOneHitPoints, $playerOneDamagePoints, $playerOneWeapon);
        $playerTwoFightInfo = new PlayerFightInfo($playerTwoInfo->getPlayer(), $playerTwoHitPoints, $playerTwoDamagePoints, $playerTwoWeapon);

        return new DuelInfo($playerOneFightInfo, $playerTwoFightInfo);
    }
}
