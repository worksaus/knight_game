<?php
declare(strict_types=1);

namespace app\command;

use app\model\service\render\ConsoleRenderer;
use app\model\tournament\TournamentService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * User: ansaus
 * Date: 16.10.2021
 */
class TournamentCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('start-tournament')
            ->addArgument('playerNumber', InputArgument::REQUIRED, 'Enter number of participants');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $renderer = new ConsoleRenderer($output);
        $tournamentService = new TournamentService($renderer);

        $tournamentService->executeTournament(intval($input->getArgument('playerNumber')));

        return Command::SUCCESS;
    }
}
