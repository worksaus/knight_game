# Task for Development

**Software developers LOVE playing games**, so you are asked to develop and play a game.

The king has called for a knights tournament, where knights will fight one on one, and all knights in the country are participating. An amount of `N` knights (user can input value `N`) will fight round-based against each other in duels. At the beginning of the competition, each knight has **100 life points**.

During each duel, two knights are selected randomly to fight against each other. Each knight must fight exactly **ONCE** per round against one other knight. A random generator (e.g., a 6-sided die) determines how many life points a knight loses in a duel. Thus, both knights will lose life points in a duel, but not every duel results in a knight’s death (not every duel is a death match).

The count of remaining life points will be stored and reused in the next round (next duel), and there is no "reset" of life points in the game. A knight who reaches **0 life points** dies and leaves the game. The last knight with positive life points wins the competition.

### Requirements

1. **Write the game**  
   This game is actually a simulation, to be implemented as a PHP CLI-based console application. The program’s progress should be clearly visible. Later, it should be possible to add a graphical user interface (e.g., a website) or move all program output to a logfile. It is **IMPORTANT** to create a clean and extendable object-oriented program structure with reusable classes, as this is the **MOST** important aspect of this task.

2. **Additional Functionalities**
   If the basic functionality of the game works (we would like to play this first version!), please add the following functionalities:

    - **Magic Shield**: Allows a knight to avoid life point loss in a duel. If the knight uses the shield, all damage is redirected to the opponent. This item can be used only once in a game. The knight will decide **WHEN** to use this item.
    - **Magic Wand**: Allows a knight to double the damage inflicted on the opponent during a duel. This item can be used only once in a game, and the knight will decide **WHEN** to use it.

   Play the game, and propose ideas for extending it. Be creative and add additional logic to make the game more exciting, complex, or random.

3. **Game Characteristics Summary**
   In this task, we aim to create:
    - An **object-oriented, round-based game**
    - Using life points for two or more players, with a **random-based life point deduction**
    - Determining the winner by eliminating knights as they reach zero life points.
