# README #

### What is this repository for? ###

* Knight tournament test cli game
* Version 1.0.0
* [Task specification](task_for_development.md)


### How do I get set up? ###

* clone git repository
* run composer install
* then run php cli start-tournament [number of knights participating in the tournament]
