<?php
/**
 * User: ansaus
 * Date: 19.10.2021
 */

namespace app\tests\model\tournament;

use app\model\character\Knight;
use app\model\tournament\DuelInfo;
use app\model\tournament\DuelService;
use app\model\tournament\PlayerFightInfo;
use app\model\weapon\BasicWeaponBehavior;
use app\model\weapon\ConstantPointWeaponBehavior;
use app\model\weapon\MagicWandWeaponBehavior;
use app\model\weapon\SelectWeaponBehavior;
use app\model\weapon\ShieldWeaponBehavior;
use PHPUnit\Framework\TestCase;

class DuelServiceTest extends TestCase
{
    protected $playerOne;
    protected $playerTwo;
    protected $duelService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->duelService = new DuelService();
        $this->playerOne = new Knight();
        $this->playerOne->setSelectWeaponBehavior(new SelectWeaponBehavior());
        $this->playerTwo = new Knight();
        $this->playerTwo->setSelectWeaponBehavior(new SelectWeaponBehavior());
    }

    public function testFightBasic()
    {
        $this->playerOne->addWeaponBehavior(new BasicWeaponBehavior());
        $this->playerTwo->addWeaponBehavior(new BasicWeaponBehavior());
        $duelInfo = $this->duelService->fight(new DuelInfo(new PlayerFightInfo($this->playerOne), new PlayerFightInfo($this->playerTwo)));
        $this->assertEquals(true, !$duelInfo->getPlayerOne()->isDead());
        $this->assertEquals(true, !$duelInfo->getPlayerTwo()->isDead());
    }

    public function testFightBasicVsShield()
    {
        $this->playerOne->addWeaponBehavior(new BasicWeaponBehavior());
        $this->playerTwo->addWeaponBehavior(new ShieldWeaponBehavior());
        $duelInfo = $this->duelService->fight(new DuelInfo(new PlayerFightInfo($this->playerOne), new PlayerFightInfo($this->playerTwo)));
        $this->assertEquals(true, ($duelInfo->getPlayerTwo()->getLifePoints() == 100));
    }

    public function testFightBasicVsMagicWand()
    {
        $this->playerOne->addWeaponBehavior(new BasicWeaponBehavior());
        $this->playerTwo->addWeaponBehavior(new MagicWandWeaponBehavior());
        $duelInfo = $this->duelService->fight(new DuelInfo(new PlayerFightInfo($this->playerOne), new PlayerFightInfo($this->playerTwo, 50)));
        $this->assertEquals(true, $duelInfo->getPlayerOne()->isDead());
    }
}
